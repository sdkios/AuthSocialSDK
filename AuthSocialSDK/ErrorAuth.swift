//
//  Error.swift
//  AppAuth
//
//  Created by Ahmed CHEBBI on 26/03/2020.
//

 
struct ErrorAuth: ErrorAuthProtocol {

    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }

    private var _description: String

    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
    
    //Cancled -> 100
    static func authCancled() -> Error {
        return ErrorAuth.init(title: "Auth Cancled", description: "You auth is cancled", code: 100)
    }
}
