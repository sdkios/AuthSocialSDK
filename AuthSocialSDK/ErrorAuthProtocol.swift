//
//  ErrorAuthProtocol.swift
//  AppAuth
//
//  Created by Ahmed CHEBBI on 26/03/2020.
//

protocol ErrorAuthProtocol: LocalizedError {

    var title: String? { get }
    var code: Int { get }
}
