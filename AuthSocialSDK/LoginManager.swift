//
//  LoginManager.swift
//  LoginDemo
//
//  Created by Ahmed CHEBBI on 04/03/2020.
//  Copyright © 2020 labsios. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit

public enum LoginType {
    case google
    case facebook
    case twitter
}

public protocol SignInDelegate {
    /// GIDSignInDelegate : The object to be notified when authentication is finished.
    /// FaceBook : The sign-in flow has finished and was successful if `error` is `nil`.
    func signIn( didSignInFor user : Dictionary<String, Any?>?, error : Error?)
    func didLogOut()
    
}
public class LoginManager: NSObject {

    public static let shared: LoginManager = LoginManager()
    //Google Parameters
    public var clientIdGoogle = ""
    
    //Facebook Parameters
    private let facebookManager = FBSDKLoginKit.LoginManager()
    
    //TwitterParams
    public var twitterWithConsumerKey = ""
    public var twitterConsumerSecret = ""

    
    //Common Parameters
    private var presentingViewController: UIViewController? //The view controller used to present
    private var delegate : SignInDelegate?
    
    public func configSignIn(type: LoginType,_ application: UIApplication? = nil,_ launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil){
        if type == .google{
            // 1 - update clientId for GIDSignIn Instance by clientIdGoogle
            GIDSignIn.sharedInstance().clientID = clientIdGoogle
            // 2 - Configure URL Schema by client ID Google (in Tab Info)
        }else if type == .facebook{
            guard let application = application else {
                return
            }
            ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        }else if type == .twitter {
            TWTRTwitter.sharedInstance().start(withConsumerKey: twitterWithConsumerKey, consumerSecret: twitterConsumerSecret)


        }
    }
    
    public func signIn(type: LoginType, permissions: [String] = ["public_profile","email"]){
        if type == .google{
            GIDSignIn.sharedInstance()?.presentingViewController = self.presentingViewController
            GIDSignIn.sharedInstance()?.signIn()
        }else if type == .facebook {
            facebookManager.logIn(permissions: permissions, from: self.presentingViewController) { (loginManagerLoginResult, error) in
                if error != nil {
                    self.delegate?.signIn(didSignInFor: nil, error : error)
                }else {
                    self.getUserFromFb()
                }
            }
        }else if type == .twitter {
            TWTRTwitter.sharedInstance().logIn(with: self.presentingViewController) { (session, error) in
                if error != nil {
                    self.delegate?.signIn(didSignInFor: nil, error : error)
                }else {
                    self.getUserFromTwitter()
                }
            }
        }
    }
    
    private func getUserFromFb(){
        if((AccessToken.current) != nil) // check if user is already loged
        {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        if let dict = result as? Dictionary<String, Any?>{
                            self.delegate?.signIn(didSignInFor: dict, error : nil)
                        }
                    }else {
                        self.delegate?.signIn(didSignInFor: nil, error: error)
                }
            })
        } else {
            self.delegate?.signIn(didSignInFor: nil, error: ErrorAuth.authCancled())
        }
    }
    
    private func getUserFromTwitter(){
            var result = Dictionary<String, Any?>()
            let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID
            let authToken = TWTRTwitter.sharedInstance().sessionStore.session()?.authToken
            let authTokenSecret = TWTRTwitter.sharedInstance().sessionStore.session()?.authTokenSecret

            let client = TWTRAPIClient.withCurrentUser()
            
            var emailUser: String?
            var firstName: String?
            var lastName: String?
            var screenName: String?
            var isVerified: Bool?
            var isProtected: Bool?
            var profileImageURL: String?
            var profileImageMiniURL: String?
            var profileImageLargeURL: String?
            var formattedScreenName: String?
            var profileURL: URL?
            
            client.requestEmail { email, error in
              emailUser = email
            }
            client.loadUser(withID: userID ?? "") { (user, error) in
                let firstLastName = user?.name.split(separator: " ")
                if (firstLastName!.count >= 2 ) {
                    lastName = String(firstLastName![1])
                    firstName =  String(firstLastName![0])
                }else {
                    lastName = (user?.name)!
                    firstName =  (user?.name)!
                }
                
                screenName = user?.screenName
                isVerified = user?.isVerified
                isProtected = user?.isProtected
                profileImageURL = user?.profileImageURL
                profileImageMiniURL = user?.profileImageMiniURL
                profileImageLargeURL = user?.profileImageLargeURL
                formattedScreenName = user?.formattedScreenName
                profileURL = user?.profileURL
                
                result["userID"] = userID
                result["authToken"] = authToken
                result["authTokenSecret"] = authTokenSecret
                result["emailUser"] = emailUser
                result["firstName"] = firstName
                result["lastName"] = lastName
                result["userName"] = screenName
                result["isVerified"] = isVerified
                result["isProtected"] = isProtected
                result["profileImageURL"] = profileImageURL
                result["profileImageMiniURL"] = profileImageMiniURL
                result["profileImageLargeURL"] = profileImageLargeURL
                result["formattedScreenName"] = formattedScreenName
                result["profileURL"] = profileURL

                self.delegate?.signIn(didSignInFor: result, error : nil)
            }
            

    }

    
    public func setPresentingViewController(view : UIViewController){
        self.presentingViewController = view
    }
    
    public func logOut(type: LoginType){
        if type == .google{
            GIDSignIn.sharedInstance()?.disconnect()
            GIDSignIn.sharedInstance()?.signOut()
        } else if type == .facebook {
            facebookManager.logOut()
        } else if type == .twitter {
            let store = TWTRTwitter.sharedInstance().sessionStore
            if let userID = store.session()?.userID {
              store.logOutUserID(userID)
            }
        }
        delegate?.didLogOut()
    }
    
    public func openUrl(type: LoginType,application : UIApplication? = nil, url : URL,options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool{
        if type == .google{
           return GIDSignIn.sharedInstance().handle(url)
        }else if type == .facebook {
            guard let application = application else {
                return false
            }
            return ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        }else if type == .twitter {
            guard let application = application else {
                return false
            }
            return TWTRTwitter.sharedInstance().application(application, open: url, options: options)
        }
        return false
    }
    
    //The delegate will be called at the end of this process indicating success or failure.
    ///   The current values of
    /// `GIDSignIn`'s configuration properties will not impact the restored user.
    public func restorePreviousSignIn(type: LoginType){
        if hasPreviousSignIn(type: type){
            if type == .google{
               GIDSignIn.sharedInstance()?.restorePreviousSignIn()
           }else if type == .facebook{
               self.getUserFromFb()
            }else if type == .twitter {
                self.getUserFromTwitter()
            }
        }
       
    }
    
    /// Checks if there is a previously authenticated user saved in keychain.
    ///
    /// @return `YES` if there is a previously authenticated user saved in keychain.
    public func hasPreviousSignIn(type: LoginType) -> Bool{
        if type == .google {
            return GIDSignIn.sharedInstance()?.hasPreviousSignIn() ?? false
        } else if type == .facebook {
            return AccessToken.isCurrentAccessTokenActive // Check the token is not expired
        } else if type == .twitter {
            return TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()
        }
        return false
    }
    
    public func setDelegate(delegate: SignInDelegate){
        GIDSignIn.sharedInstance()?.delegate = self
        self.delegate = delegate
    }
}
extension LoginManager: GIDSignInDelegate {
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil {
            self.delegate?.signIn(didSignInFor: nil , error: error)
        }else {
            var result = Dictionary<String, Any?>()
            result["userID"] = user.authentication.clientID
            result["authToken"] = user.authentication.accessToken
            result["authTokenSecret"] = user.authentication.accessToken
            result["emailUser"] = user.profile.email
            result["firstName"] = user.profile.name
            result["lastName"] = user.profile.familyName
            result["userName"] = user.profile.givenName
            result["profileImageURL"] = user.profile.imageURL(withDimension: 200)
            result["profileImageMiniURL"] = user.profile.imageURL(withDimension: 200)
            result["profileImageLargeURL"] = user.profile.imageURL(withDimension: 200)
            self.delegate?.signIn(didSignInFor: result, error: error)
        }

    }
    public func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        self.delegate?.didLogOut()
    }
    
}
